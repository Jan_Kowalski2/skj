import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class Helpers {

    private static final String INIT_DIR = "C:/sandbox/";
    private static final String HOST_DIR = "host_";
    private static final int TIMEOUT = 200;
    private static final int INIT_SOCKET = 10001;
    public static final int BUFFER = 8192;

    public static int getFreeHostNumber() {

        boolean isSocketAvailable = false;
        int num = INIT_SOCKET;

        while (!isSocketAvailable) {
            try(Socket socket = new Socket()) {
                socket.connect(new InetSocketAddress(num), TIMEOUT);
                num++;
            } catch (IOException e) {
                isSocketAvailable = true;
            }
        }

        return num;
    }

    public static List<Integer> getAvailableHosts() {

        int connectionAttempt = 10;

        List hosts = new ArrayList<Integer>();
        int hostNumber = INIT_SOCKET;

        while (connectionAttempt >= 0) {
            try(Socket socket = new Socket()) {
                socket.connect(new InetSocketAddress(hostNumber), TIMEOUT);
                hosts.add(hostNumber);
            } catch (IOException e) {
                connectionAttempt--;
            }
            hostNumber++;
        }

        return hosts;
    }

    public static void listHosts(int currentPort) {

        boolean isSocketActive = false;
        int additionalHosts = 10;
        int num = INIT_SOCKET;

        while (!isSocketActive || additionalHosts > 0) {
            if (num == currentPort) {
                System.out.println("Host: " + num + " (you).");
            }
            else {
                try(Socket socket = new Socket()) {
                    socket.connect(new InetSocketAddress(num), TIMEOUT);
                    System.out.println("Host: " + num + ".");
                } catch (IOException e) {
                    isSocketActive = true;
                    additionalHosts--;
                }
            }
            num++;
        }
    }

    public static void createHostDirectoryIfNotExists(int num) {
        File rootDirectory = new File(INIT_DIR);

        if (!rootDirectory.exists() || !rootDirectory.isDirectory()) {
            new File(INIT_DIR).mkdir();
        }


        String directoryName = INIT_DIR + HOST_DIR + num;
        File directory = new File(directoryName);

        if (!directory.exists() || !directory.isDirectory()) {
            new File(directoryName).mkdir();
        }
    }

    public static byte[] createChecksumFromFile(String fileName) throws Exception {
        InputStream fileInputStream = new FileInputStream(fileName);
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        byte[] buffer = new byte[1024];
        int numRead;

        do {
            numRead = fileInputStream.read(buffer);
            if (numRead > 0) {
                messageDigest.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fileInputStream.close();

        return messageDigest.digest();
    }

    public static String getMD5Checksum(String fileName) throws Exception {
        byte[] bytes = createChecksumFromFile(fileName);

        String result = "";

        for (int i = 0; i < bytes.length; i++) {
            result = result.concat(Integer.toString(( bytes[i] & 0xff ) + 0x100, 16).substring(1));
        }

        return result;
    }


    public static void concatFile(String fileName, int parts) throws IOException, InterruptedException {

        OutputStream out = new FileOutputStream(fileName);
        byte[] buffer = new byte[BUFFER];

        for (int i = 0; i < parts; i++) {
            InputStream in = new FileInputStream(fileName + ".part" + (i + 1));
            int count;

            while ((count = in.read(buffer)) > 0) {
                out.write(buffer, 0, count);
            }

            in.close();

        }

        out.close();

        Thread.sleep(TIMEOUT);

        for (int i = 0; i < parts; i++) {
            Files.deleteIfExists(Paths.get(fileName + ".part" + (i + 1)));
        }
    }

    public static long getFileBufferSize(int port, String fileName) {
        final File file = new File(INIT_DIR + HOST_DIR + port + File.separator + fileName);
        long unfinishedSegment = (int) (file.length() % BUFFER);
        long finishedSegments = (file.length() - unfinishedSegment) / BUFFER;

        return finishedSegments;
    }

    public static long getFileFinishedBufferSize(File file) {
        long lastSegment = (int) (file.length() % BUFFER) == 0 ? 0 : 1;
        long finishedSegments = (file.length() - lastSegment) / BUFFER;

        return finishedSegments + lastSegment;
    }

    public static String listFiles(int port) {
        final String pathName = INIT_DIR + HOST_DIR + port;
        final File folder = new File(pathName);
        String message = "";

        for (final File fileEntry : folder.listFiles()) {
            try {
                String md5 = Helpers.getMD5Checksum(pathName + File.separator + fileEntry.getName());
                message = message.concat("| " + fileEntry.getName() + ", " + md5 + " ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return message.length() > 0 ? message : "No files inside.";
    }

    public static boolean isPortActive(int port) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(port), TIMEOUT);
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public static boolean isFileExists(int port, String fileName) {
        final String pathName = INIT_DIR + HOST_DIR + port;
        final File file = new File(pathName + File.separator + fileName);

        if (file.exists() && !file.isDirectory()) {
            return true;
        }

        return false;

    }
}

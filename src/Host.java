import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.*;

public class Host {

    public static final String INIT_DIR = "C:/sandbox/";
    public static final String HOST_DIR = "host_";
    public static final int TIMEOUT = 200;
    public static final int BUFFER = 8192;

    private int PORT;

    public Host(int port){
        this.PORT = port;

        System.out.println("Piotr Kwiatek, s18033, 22c.");

        Helpers.createHostDirectoryIfNotExists(port);

        this.startServer();
        this.startClient();
    }

    private void sendMessage(Socket socket, String message) throws IOException {
        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        output.write(message);
        output.newLine();
        output.flush();
    }

    private void sendFile(Socket socket, String fileName, int beginOffset, long end) throws IOException {
        try {
            File file = new File(INIT_DIR + HOST_DIR + PORT + File.separator + fileName);

            if (end < 0) {
                end = Helpers.getFileFinishedBufferSize(file);
            }

            InputStream in = Files.newInputStream(file.toPath());
            OutputStream out = socket.getOutputStream();

            int count;
            int bufferCounter = 0;
            byte[] buffer = new byte[BUFFER];
            while ((count = in.read(buffer)) > 0) {
                if (bufferCounter >= beginOffset && bufferCounter < end) {
                    out.write(buffer, 0, count);
                }
                bufferCounter++;
            }

            out.close();
            System.out.println("File sent successfully.");
        } catch(IOException e) {
            socket.close();
            System.out.println("There was an error during sending file: " + e);
        }

    }

    private void getFile(Socket socket, String fileName, boolean append, boolean isSinglePartDownload) throws IOException {
        InputStream in = socket.getInputStream();
        int byteToBeRead = -1;

        String pathName = INIT_DIR + HOST_DIR + PORT + File.separator + fileName;
        String pathNameWithoutPart = pathName.substring(0, pathName.length() - 6);

        File file = new File(pathName);

        FileOutputStream fileOutputStream = new FileOutputStream(file, append);
        while((byteToBeRead = in.read())!= -1) {
            fileOutputStream.write(byteToBeRead);
        }

        fileOutputStream.flush();
        fileOutputStream.close();

        if (append == true) {
            try {
                Helpers.concatFile(pathNameWithoutPart, 2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (isSinglePartDownload == true) {
            file.renameTo(new File(pathNameWithoutPart));
        }

    }

    private int checkPort(String message, int offset) {

        int port = 0;

        if (message.length() < offset) {
            return -1;
        }
        else {
            try {
                port = Integer.parseInt(message.substring(offset));
            } catch (NumberFormatException e) {
                System.out.println("Unknown port number format.");
                return -1;
            }
        }

        if (!Helpers.isPortActive(port)) {
            System.out.println("Host in this port is not active.");
            return -1;
        }

        return port;

    }

    private int[] findHostsWithFile(String fileName) {

        List hosts = Helpers.getAvailableHosts();
        Iterator<Integer> hostsIterator = hosts.iterator();
        List<Integer> hostsWithFile = new ArrayList<>();

        while(hostsIterator.hasNext()) {

            Integer hostNumber = hostsIterator.next();

            try {
                Socket socket = new Socket("localhost", hostNumber);
                this.sendMessage(socket, "IS FILE " + fileName);

                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String line = null;
                String message = "";

                while (!socket.isClosed() && (line = in.readLine()) != null) {
                    message = line;
                    socket.close();
                }

                if (message.equals("OK")) {
                    hostsWithFile.add(hostNumber);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        int[] arr = new int[hostsWithFile.size()];
        for (int i = 0; i < hostsWithFile.size(); i++) {
            arr[i] = hostsWithFile.get(i);
        }

        return arr;
    }

    private void startServer() {
        (new Thread(() -> {
            ServerSocket serverSocket;
            try {
                serverSocket = new ServerSocket(PORT);

                while(true) {
                    Socket clientSocket = serverSocket.accept();

                    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    String line = null;

                    while (!clientSocket.isClosed() && (line = in.readLine()) != null) {
                        if (line.startsWith("PART")) {

                            int beginOffsetStart = line.indexOf("PART") + 5;
                            int beginOffsetEnd = line.indexOf("END") - 1;
                            int endOffsetStart = line.indexOf("END") + 4;
                            int endOffsetEnd = line.indexOf("SEND") - 1;

                            String fileName = line.substring(line.indexOf("SEND") + 5);
                            String begin = line.substring(beginOffsetStart, beginOffsetEnd);
                            String end = line.substring(endOffsetStart, endOffsetEnd);


                            int beginOffset = Integer.parseInt(begin);
                            int endOffset = Integer.parseInt(end);

                            this.sendFile(clientSocket, fileName, beginOffset, endOffset);
                        }
                        if (line.startsWith("SEND")) {
                            this.sendFile(clientSocket, line.substring(5), 0, -1);
                        }
                        if (line.startsWith("SIZEOF")) {
                            String fileName = line.substring(7);
                            File file = new File(INIT_DIR + HOST_DIR + PORT + File.separator + fileName);
                            String fileSegments = String.valueOf(Helpers.getFileFinishedBufferSize(file));
                            this.sendMessage(clientSocket, fileSegments);
                        }
                        if (line.startsWith("RECEIVE")) {
                            this.getFile(clientSocket, line.substring(8), false, false);
                        }
                        if (line.startsWith("IS FILE")) {
                            String fileName = line.substring(8);
                            if (Helpers.isFileExists(PORT, fileName)) {
                                this.sendMessage(clientSocket, "OK");
                            }
                            else {
                                this.sendMessage(clientSocket, "NO");
                            }
                        }
                        if (line.startsWith("LIST FILES")) {
                            this.sendMessage(clientSocket, Helpers.listFiles(PORT));
                        }
                    }

                    clientSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        })).start();
    }

    private void startClient() {
        (new Thread(() -> {
            try {
                Scanner scanner = new Scanner(System.in);

                while (true) {
                    System.out.print("Host" + PORT + " $: ");
                    String command = scanner.nextLine().toUpperCase();

                    if (command.equals("EXIT")) {
                        break;
                    }

                    else if (command.equals("PULL FILE")) {
                        System.out.println("Which file you are looking for?");
                        String fileName = scanner.next();

                        int[] hosts = this.findHostsWithFile(fileName);

                        if (hosts.length > 0) {
                            Socket firstSocket = new Socket("localhost", hosts[0]);

                            this.sendMessage(firstSocket, "SIZEOF " + fileName);

                            BufferedReader in = new BufferedReader(new InputStreamReader(firstSocket.getInputStream()));
                            String line = null;
                            String message = "";
                            long fileSegments = 12345678910L;

                            while (!firstSocket.isClosed() && (line = in.readLine()) != null) {
                                fileSegments = Long.parseLong(line);
                                firstSocket.close();
                            }

                            long partSize = fileSegments / hosts.length;

                            int i = 0;

                            boolean[] isPartDownloaded = new boolean[hosts.length];

                            while (i < hosts.length) {
                                int finalPartNum = i;
                                new Thread(() -> {
                                    long start = finalPartNum * partSize;
                                    long end = finalPartNum == hosts.length - 1 ? -1 : (finalPartNum + 1) * partSize;

                                    try {
                                        Socket partSocket = new Socket("localhost", hosts[finalPartNum]);
                                        this.sendMessage(partSocket, "PART " + start + " END " + end + " SEND " + fileName);
                                        this.getFile(partSocket, fileName + ".part" + (finalPartNum + 1), false, false);

                                        isPartDownloaded[finalPartNum] = true;
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }).start();
                                i++;
                            }

                            boolean isEveryPartDownloaded = false;

                            try {
                                while(!isEveryPartDownloaded) {
                                    Thread.sleep(TIMEOUT * 2);

                                    for (int j = 0; j < isPartDownloaded.length; j++) {
                                        if (isPartDownloaded[j]) {
                                            isEveryPartDownloaded = true;
                                        }
                                    }
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            try {
                                String pathName = INIT_DIR + HOST_DIR + PORT + File.separator + fileName;
                                System.out.println(pathName);
                                Helpers.concatFile(pathName, hosts.length);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        else {
                            System.out.println("No file found.");
                        }


                    }

                    else if (command.startsWith("PULL FILE FROM HOST")) {
                        System.out.println("From which host do you want to download file?");
                        String port = scanner.next();
                        int portNumber = checkPort(port ,0);

                        if (portNumber > 0) {
                            System.out.println("Okay. Which file? Type name with extension.");
                            String fileName = scanner.next();

                            Socket socket = new Socket("localhost", portNumber);

                            this.sendMessage(socket, "IS FILE " + fileName);
                            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            String line = null;
                            String message = "";

                            while (!socket.isClosed() && (line = in.readLine()) != null) {
                                message = line;
                                socket.close();
                            }

                            if (message.equals("OK")) {
                                socket = new Socket("localhost", portNumber);
                                if (Helpers.isFileExists(PORT, fileName + ".part1")) {
                                    long beginOffset = Helpers.getFileBufferSize(PORT, fileName + ".part1");
                                    this.sendMessage(socket, "PART " + beginOffset + " END -1 SEND " + fileName);
                                    this.getFile(socket, fileName + ".part2", true, false);
                                }
                                else {
                                    this.sendMessage(socket, "SEND " + fileName);
                                    this.getFile(socket, fileName + ".part1", false, true);
                                }
                            }
                            else {
                                System.out.println("File doesn't exists on this host.");
                            }

                        }
                    }

                    else if (command.startsWith("PUSH FILE TO HOST")) {
                        System.out.println("To which host do you want to upload file?");
                        String port = scanner.next();
                        int portNumber = checkPort(port ,0);

                        if (portNumber > 0) {
                            System.out.println("Okay. Which file? Type name with extension.");
                            String fileName = scanner.next();

                            if (Helpers.isFileExists(PORT, fileName)) {
                                Socket socket = new Socket("localhost", portNumber);

                                    this.sendMessage(socket, "RECEIVE " + fileName);
                                this.sendFile(socket, fileName, 0, -1);
                            }

                            else {
                                System.out.println("There is no such file.");
                            }

                        }
                    }

                    else if (command.startsWith("LIST FILES")) {
                        int port = checkPort(command, 11);

                        if (port > 0) {
                            Socket socket = new Socket("localhost", port);

                            this.sendMessage(socket, command);

                            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            String line = null;

                            while (!socket.isClosed() && (line = in.readLine()) != null) {
                                System.out.println(line);
                                socket.close();
                            }
                        }
                    }

                    else if (command.startsWith("LIST HOSTS")) {
                        Helpers.listHosts(PORT);
                    }

                    else if (command.startsWith("COMMAND")) {
                        System.out.println("List files <port>, List hosts, push file to host, pull file, pull file from host, commands, exit.");
                    }

                    else if (command.length() <= 1) {
                        System.out.println("");
                    }

                    else {
                        System.out.println("Unknown command. Type commands to show available commands.");
                    }

                    Thread.sleep(TIMEOUT);
                }

                scanner.close();
                System.exit(0);

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        })).start();
    }
}